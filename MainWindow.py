import os
import time
import datetime
from PyQt5 import QtWidgets
import operator
from . import FindHoldingsWindow

class MainWindow(QtWidgets.QWidget):
    def __init__(self, LSM):
        super(MainWindow, self).__init__()
        self.LSM = LSM
        self.setWindowTitle("IZ MMS ID to LIS")
        self.layout = QtWidgets.QGridLayout()
        self.InputLabel = QtWidgets.QLabel("Input File: ")
        self.layout.addWidget(self.InputLabel, 0, 0)
        self.Input = QtWidgets.QLabel(self.LSM.InputFile[0])
        self.layout.addWidget(self.Input, 0, 1)
        self.OutputLabel = QtWidgets.QLabel("Output File: ")
        self.layout.addWidget(self.OutputLabel, 1, 0)
        if self.LSM.OutputFile[0].find(".csv") == -1:
            self.Output = QtWidgets.QLabel(self.LSM.OutputFile[0] + ".csv")
        else:
            self.Output = QtWidgets.QLabel(self.LSM.OutputFile[0])
        self.layout.addWidget(self.Output, 1, 1)
        self.ReportContainsLabel = QtWidgets.QLabel("Report Should Contain Items with ")
        self.layout.addWidget(self.ReportContainsLabel, 2,0)
        self.ReportContainsLayout = QtWidgets.QHBoxLayout()
        self.ReportContains = QtWidgets.QComboBox()
        self.ReportContains.addItem("less than")
        self.ReportContains.addItem("greater than")
        self.ReportContains.addItem("equal to")
        self.ReportContainsLayout.addWidget(self.ReportContains)
        self.ReportContainsNumber = QtWidgets.QLineEdit()
        self.ReportContainsNumber.setText(str(self.LSM.HoldingsComparison))
        self.ReportContainsLayout.addWidget(self.ReportContainsNumber)
        self.ReportContainsLabelTwo = QtWidgets.QLabel(" Holdings")
        self.ReportContainsLayout.addWidget(self.ReportContainsLabelTwo)
        self.layout.addLayout(self.ReportContainsLayout, 2, 1)
        self.IncludeTitle = QtWidgets.QCheckBox("Include Title (Field 245a)")
        self.IncludeTitle.setChecked(self.LSM.IncludeTitle)
        self.layout.addWidget(self.IncludeTitle, 3, 1)
        self.IncludeOCLC = QtWidgets.QCheckBox("Include OCLC (Field 035a)")
        self.IncludeOCLC.setChecked(self.LSM.IncludeOCLC)
        self.layout.addWidget(self.IncludeOCLC, 4, 1)
        self.IncludeNumberOfHoldings = QtWidgets.QCheckBox("Include Number of Holdings")
        self.IncludeNumberOfHoldings.setChecked(self.LSM.IncludeNumberOfHoldings)
        self.layout.addWidget(self.IncludeNumberOfHoldings, 5, 1)
        self.IncludeNZMMSID = QtWidgets.QCheckBox("Include Network MMS ID")
        self.IncludeNZMMSID.setChecked(self.LSM.IncludeNZMMSID)
        self.layout.addWidget(self.IncludeNZMMSID, 6, 1)
        self.IncludeIZMMSID = QtWidgets.QCheckBox("Include Institution MMS ID")
        self.IncludeIZMMSID.setChecked(self.LSM.IncludeIZMMSID)
        self.layout.addWidget(self.IncludeIZMMSID, 7, 1)
        self.CancelButton = QtWidgets.QPushButton("Cancel")
        self.CancelButton.clicked.connect(self.close)
        self.layout.addWidget(self.CancelButton, 100, 0)
        self.GoButton = QtWidgets.QPushButton("Go!")
        self.GoButton.clicked.connect(self.CreateFindHoldingsWindow)
        self.layout.addWidget(self.GoButton, 100, 1)
        self.setLayout(self.layout)

    def CreateFindHoldingsWindow(self):
#       TODO: Add more things to put in the report.
        self.LSM.IncludeTitle = self.IncludeTitle.isChecked()
        self.LSM.IncludeNumberOfHoldings = self.IncludeNumberOfHoldings.isChecked()
        self.LSM.IncludeOCLC = self.IncludeOCLC.isChecked()
        self.LSM.IncludeIZMMSID = self.IncludeIZMMSID.isChecked()
        self.LSM.IncludeNZMMSID = self.IncludeNZMMSID.isChecked()
        if (self.ReportContains.currentIndex() == 0):
            self.LSM.Comparator = operator.lt
        elif (self.ReportContains.currentIndex() == 1):
            self.LSM.Comparator = operator.gt
        elif (self.ReportContains.currentIndex() == 2):
            self.LSM.Comparator = operator.eq
        self.LSM.HoldingsComparison = int(self.ReportContainsNumber.text())
        self.FindHoldingsWindow = FindHoldingsWindow.FindHoldingsWindow(self.LSM)
        #self.ReportContainsNumber.setText(self.LSM.HoldingsComparison)
        self.close()
        self.FindHoldingsWindow.show()

