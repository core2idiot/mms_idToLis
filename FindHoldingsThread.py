from PyQt5 import QtWidgets
from PyQt5.QtCore import QThread, pyqtSignal

class FindHoldingsThread(QThread):
    ListCompleted = pyqtSignal(int)
    ItemCompleted = pyqtSignal()
    AllItemsCompleted = pyqtSignal()
    Notice = pyqtSignal(str)
    FatalError = pyqtSignal(str)
    def __init__(self, LSM):
        QThread.__init__(self)
        self.LSM = LSM

    def run(self):
        if self.LSM.OutputFile[0].find(".csv") == -1:
            self.Output = self.LSM.OutputFile[0] + ".csv"
        else:
            self.Output = self.LSM.OutputFile[0]
        self.Input = self.LSM.InputFile[0]
        self.InputCount = 0
        self.LSM.write_to_log("Found Input and Output files: " + self.Input + ", " + self.Output,  5)
        self.InputFile = open(self.Input, "r")
        for line in self.InputFile:
            self.InputCount += 1
        self.ListCompleted.emit(self.InputCount)
        self.InputFile.close()
        self.InputFile = open(self.Input, "r")
        self.OutputFile = open(self.Output, "w")
        if self.LSM.IncludeIZMMSID:
            self.OutputFile.write("IZ MMSID, ")
        if self.LSM.IncludeNZMMSID:
            self.OutputFile.write("NZ MMSID, ")
        if self.LSM.IncludeTitle:
            self.OutputFile.write("Title, ")
        if self.LSM.IncludeOCLC:
            self.OutputFile.write("OCLC, ")
        if self.LSM.IncludeNumberOfHoldings:
            self.OutputFile.write("Number of Holdings, ")
        self.OutputFile.write("\n")
        for line in self.InputFile:
            done = False
            try:
                mmsid = line.replace("\n", "").replace(",", " ")
                holdings = self.LSM.FindCurrentAllianceHoldingsBYIZ_MMSID(line)
                if self.LSM.Comparator(len(holdings), self.LSM.HoldingsComparison):
                    SRUResponse = self.LSM.GetSRUsearchRetrieveByMMSIDInstitution(line)
                    if self.LSM.IncludeIZMMSID:
                        self.OutputFile.write(mmsid + ", ")
                        done = True
                    if self.LSM.IncludeNZMMSID:
                        nzmmsid = self.LSM.findNZ_MMSIDByIZ_MMSID(mmsid)
                        self.OutputFile.write(nzmmsid.replace(",", " ") + ", ")
                        done = True
                    if self.LSM.IncludeTitle:
                        self.OutputFile.write(self.LSM.GetSRUAttribute(SRUResponse, "245", "a")[0].text.replace(",", " ") + ", ")
                        done = True
                    if self.LSM.IncludeOCLC:
                        self.OutputFile.write(self.LSM.GetSRUAttribute(SRUResponse, "035", "a")[0].text.replace(",", " ") + ", ")
                        done = True
                    if self.LSM.IncludeNumberOfHoldings:
                        self.OutputFile.write(str(len(holdings)) + ", ")
                        done = True

            except Exception as e:
                self.Notice.emit("ERROR on " + mmsid + " " + str(e))
            self.ItemCompleted.emit()
            if done:
                self.OutputFile.write("\n")


        self.InputFile.close()
        self.OutputFile.close()
        self.AllItemsCompleted.emit()
