from PyQt5 import QtWidgets
from . import FindHoldingsThread

class FindHoldingsWindow(QtWidgets.QWidget):
    #ListCompleted = pyqtSignal(int)
    #ItemCompleted = pyqtSignal()
    #Notice = pyqtSignal(str)
    #FatalError = pyqtSignal(str)
    def __init__(self, LSM):
        super(FindHoldingsWindow, self).__init__()
        self.NumberOfItems = 0
        self.ItemsDone = 0
        self.LSM = LSM
        self.FindHoldings = FindHoldingsThread.FindHoldingsThread(self.LSM)
        self.layout = QtWidgets.QGridLayout()
        self.ErrorList = QtWidgets.QListWidget()
        self.cancel = QtWidgets.QPushButton("Cancel")
        self.cancel.clicked.connect(self.Cancel)
        self.NumericalProgress = QtWidgets.QLabel("xx/xx Completed")
        self.progress = QtWidgets.QProgressBar()
        self.progress.setValue(0)
        self.save = QtWidgets.QPushButton("Save Log")
        self.save.clicked.connect(self.SaveLog)
        self.setLayout(self.layout)
        self.layout.addWidget(self.cancel, 3,0)
        self.layout.addWidget(self.ErrorList, 1, 0)
        self.layout.addWidget(self.NumericalProgress, 2, 0)
        self.layout.addWidget(self.progress, 0, 0)
        self.FindHoldings.ListCompleted.connect(self.FindHoldingsListCompleted)
        self.FindHoldings.ItemCompleted.connect(self.FindHoldingsUpdateProgress)
        self.FindHoldings.Notice.connect(self.NoticeReceive)
        self.FindHoldings.AllItemsCompleted.connect(self.AllItemsCompleted)
        self.FindHoldings.start()

    def AllItemsCompleted(self):
        self.LSM.write_to_log("Report Completed: " + self.FindHoldings.Output + " with " + str(self.ErrorList.count()) + " Errors", 1)
        if self.ErrorList.count() > 0:
            self.layout.addWidget(self.save)
            self.cancel.setText("Close")
        else:
            self.close()

    def NoticeReceive(self, string):
        self.ErrorList.addItem(string)

    def FindHoldingsUpdateProgress(self):
        self.ItemsDone += 1
        self.NumericalProgress.setText(str(self.ItemsDone) + "/" + str(self.NumberOfItems) + " Completed, " + str(self.ErrorList.count()) + " errors")
        self.progress.setValue((self.ItemsDone/self.NumberOfItems) * 100)

    def Cancel(self):
        self.FindHoldings.quit()
        self.close()

    def SaveLog(self):
        DestinationLog = QtWidgets.QFileDialog.getSaveFileName(self, "Save Log File", "", "Text files (*.txt)")
        if DestinationLog[0].find(".txt") == -1:
            DestinationLogStr = DestinationLog[0] + ".txt"
        else:
            DestinationLogStr = DestinationLog[0]
        LogOut = open(DestinationLogStr, "w")
        for idx in range(0, self.ErrorList.count()):
            LogOut.write(self.ErrorList.item(idx).text() + "\n")
        LogOut.close()
        self.LSM.write_to_log("Wrote LogFile to " + DestinationLogStr, 1)

    def FindHoldingsListCompleted(self, Qty):
        self.NumberOfItems = Qty
        self.NumericalProgress.setText("0/" + str(self.NumberOfItems) + " Completed")
