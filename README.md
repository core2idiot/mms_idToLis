# MSS_ID to LIS

This Library Support Package for AlmaManager supports taking a list of MMS IDs and learning information about them based upon the number of holdings that MMSID has.

This is particularly useful for libraries that need to prune their collection, for example removing items that aren't last in summit to permit more room for new materials.
