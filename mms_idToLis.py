import os
import time
import datetime
from PyQt5 import QtWidgets
from Types.LibrarySupportPackage import LibrarySupportPackage
from . import MainWindow
class mms_idToLis(LibrarySupportPackage):
    def __init__(self):
        super(mms_idToLis, self).__init__()
        self.setTitle("IZ MMS ID to LIS")
        self.addAction("Generate Report", self.GenerateReport)
        self.InputFile = None
        self.OutputFile = None
        self.IncludeTitle = True
        self.IncludeNumberOfHoldings = False
        self.IncludeOCLC = False
        self.IncludeNZMMSID = False
        self.IncludeIZMMSID = True
        self.Comparator = None
        self.HoldingsComparison = 0



    def GenerateReport(self):
        self.InputFile = QtWidgets.QFileDialog.getOpenFileName(self, "Open Input File", "", "Comma Separated Values (*.csv)")
        self.OutputFile =  QtWidgets.QFileDialog.getSaveFileName(self, "Choose Output File", "", "Comma Separated Values (*.csv)")
        self.MainWindow = MainWindow.MainWindow(self)
        self.MainWindow.show()
        #self.write_to_log("This Should be a report", 1)

